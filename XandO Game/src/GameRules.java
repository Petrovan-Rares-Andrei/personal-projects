

import javax.swing.*;

public class GameRules {

    String winner;
    public boolean Rules(JButton[][] b)
    {


        for (int i = 0; i <b.length ; i++) {
            if (b[0][i].getText().equalsIgnoreCase("O") && b[1][i].getText().equalsIgnoreCase("O") && b[2][i].getText().equalsIgnoreCase("O")) {
                winner = "O";
                return true;
            }
            if (b[i][0].getText().equalsIgnoreCase("O") && b[i][1].getText().equalsIgnoreCase("O") && b[i][2].getText().equalsIgnoreCase("O")) {
                winner = "O";
                return true;
            }
            if (b[0][i].getText().equalsIgnoreCase("X") && b[1][i].getText().equalsIgnoreCase("X") && b[2][i].getText().equalsIgnoreCase("X")) {
                winner = "X";
                return true;
            }
            if (b[i][0].getText().equalsIgnoreCase("X") && b[i][1].getText().equalsIgnoreCase("X") && b[i][2].getText().equalsIgnoreCase("X")) {
                winner = "X";
                return true;
            }
        }
        if (b[0][0].getText().equalsIgnoreCase("O") && b[1][1].getText().equalsIgnoreCase("O") && b[2][2].getText().equalsIgnoreCase("O")) {
            winner = "O";
            return true;
        }
        if (b[0][0].getText().equalsIgnoreCase("X") && b[1][1].getText().equalsIgnoreCase("X") && b[2][2].getText().equalsIgnoreCase("X")) {
            winner = "X";
            return true;
        }
        if (b[0][2].getText().equalsIgnoreCase("O") && b[1][1].getText().equalsIgnoreCase("O") && b[2][0].getText().equalsIgnoreCase("O")) {
            winner = "O";
            return true;
        }
        if (b[0][2].getText().equalsIgnoreCase("X") && b[1][1].getText().equalsIgnoreCase("X") && b[2][0].getText().equalsIgnoreCase("X")) {
            winner = "X";
            return true;
        }

        return false;



    }

}
