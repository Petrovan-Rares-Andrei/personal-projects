import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class X_O extends JFrame  implements ActionListener {

    private  JButton[][] board;
    private boolean change_player=true;
    private  JButton reset;
    private GameRules g;
    private JLabel msg;
    private int moves=0;
    X_O()
    {

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setTitle("X&O");
        setSize(550 ,600);
        setLayout(null);
        createboard();
        for (int i=0;i<board.length;i++)
            for (int j=0;j<board.length;j++)
            {
                board[i][j].addActionListener(this);
                board[i][j].setFocusPainted(false);
            }
        reset.addActionListener(this);
        setVisible(true);
    }
    public void createboard() {

        int x_poz = 50, y_poz = 50, width = 100, height = 100;
        board = new JButton[3][3];
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board.length; j++) {
                board[i][j] = new JButton();
                board[i][j].setBounds(x_poz, y_poz, width, height);
                add(board[i][j]);
                y_poz = y_poz + 150;
            }
            y_poz = 50;
            x_poz = x_poz + 150;

        }
        reset=new JButton("RESET");
        reset.setBounds(200,10,100,30);
        add(reset);

        g= new GameRules();
        msg= new JLabel();
        msg.setBounds(50,500,400,40);

        add(msg);

    }
    @Override
    public void actionPerformed(ActionEvent e)
    {
        moves++;
        for(int i=0;i<board.length;i++)
            for(int j=0;j<board.length;j++)
            {
                if( board[i][j]==e.getSource() )
                {
                    if(change_player)
                    {
                        board[i][j].setText("X");
                        board[i][j].setFont(new Font ("TimesRoman", Font.BOLD | Font.ITALIC, 40));
                        board[i][j].setBackground(Color.RED);
                        board[i][j].setEnabled(false);
                        change_player=false;
                    }
                    else if(!change_player)
                    {
                        board[i][j].setText("O");
                        board[i][j].setFont(new Font ("TimesRoman", Font.BOLD | Font.ITALIC, 40));
                        board[i][j].setBackground(Color.BLUE);
                        board[i][j].setEnabled(false);
                        change_player=true;
                    }
                }
            }
        if(e.getSource()==reset)Reset();
        boolean winner=g.Rules(board);
        if(winner)
        {
            msg.setText(g.winner+" is the winner!");
            msg.setFont(new Font ("TimesRoman", Font.BOLD | Font.ITALIC, 40));
            for (int i = 0; i <board.length ; i++)
                for (int j = 0; j <board.length ; j++) {
                    board[i][j].setEnabled(false);

                }
        }else if(moves==9) {

            msg.setFont(new Font ("TimesRoman", Font.BOLD | Font.ITALIC, 40));
            msg.setText("It's a draw...");

            moves=0;}
    }
    public void Reset()
    {   moves=0;
        msg.setText("");
        for(int i=0;i<board.length;i++)
            for(int j=0;j<board.length;j++) {
                board[i][j].setText("");
                board[i][j].setEnabled(true);
                board[i][j].setBackground(null);
            }

    }
    public static void main(String[] args) {
        new X_O();
    }

}