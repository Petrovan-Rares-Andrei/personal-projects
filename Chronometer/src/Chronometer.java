import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Chronometer extends JFrame implements ActionListener {
    JButton b1,b2,b3;
    JLabel h,m,s,ms,l1,l2,l3;//l1,l2,l3 spacers ":" between textfields;
    JTextField hour,min,sec,mlsec;

    int hourval=0,minval=0,secval=0,mlsecval=0;
    boolean state=true,b1state=false;

    Thread t;

    Chronometer() {
        loadFrame();
    }

    public void loadFrame()
    {

        setLayout(null);
        setTitle("Chronometer");
        setSize(300,250);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        b1=new JButton("Start");
        b1.setBounds(20,180,70,20);
        b1.addActionListener(this);
        add(b1);
        b2=new JButton("Stop");
        b2.setBounds(110,180,60,20);
        b2.addActionListener(this);
        add(b2);
        b3=new JButton("Reset");
        b3.setBounds(180,180,70,20);
        b3.addActionListener(this);
        add(b3);
        hour=new JTextField("0");
        hour.setEditable(false);
        hour.setFont(new Font ("TimesRoman", Font.BOLD , 20));
        hour.setBounds(10,50,50,50);
        add(hour);
        min=new JTextField("0");
        min.setEditable(false);
        min.setFont(new Font ("TimesRoman", Font.BOLD, 20));
        min.setBounds(80,50,50,50);
        add(min);
        sec=new JTextField("0");
        sec.setEditable(false);
        sec.setFont(new Font ("TimesRoman", Font.BOLD, 20));
        sec.setBounds(150,50,50,50);
        add(sec);
        mlsec=new JTextField("0");
        mlsec.setEditable(false);
        mlsec.setFont(new Font ("TimesRoman", Font.BOLD, 20));
        mlsec.setBounds(220,50,50,50);
        add(mlsec);
        h=new JLabel("h");
        h.setFont(new Font ("TimesRoman", Font.BOLD, 15));
        h.setBounds(25,20,20,20);
        add(h);
        m=new JLabel("min");
        m.setFont(new Font ("TimesRoman", Font.BOLD, 15));
        m.setBounds(90,20,30,20);
        add(m);
        s=new JLabel("sec");
        s.setFont(new Font ("TimesRoman", Font.BOLD, 15));
        s.setBounds(160,20,30,20);
        add(s);
        ms=new JLabel("ms");
        ms.setFont(new Font ("TimesRoman", Font.BOLD, 15));
        ms.setBounds(235,20,20,20);
        add(ms);
        l1=new JLabel(":");
        l2=new JLabel(":");
        l3=new JLabel(":");
        l1.setFont(new Font("TimesRoman", Font.BOLD, 30));
        l2.setFont(new Font("TimesRoman", Font.BOLD, 30));
        l3.setFont(new Font("TimesRoman", Font.BOLD, 30));
        l1.setBounds(65,55,30,30);
        l2.setBounds(135,55,30,30);
        l3.setBounds(205,55,30,30);
        add(l1);
        add(l2);
        add(l3);

        setVisible(true);
    }
    @Override
    public  void actionPerformed(ActionEvent e)
    {
        if(e.getSource()==b1 && !b1state)
        {b1state=true;
            state=true;

            t=new Thread()
            {

                public void run()
                {
                    for(;;)
                    {
                        if(state)
                        {
                            try {
                                sleep(1);
                                mlsecval++;
                                if(mlsecval>1000)
                                {
                                    secval++;
                                    mlsecval=0;
                                }
                                if(secval>60)
                                {
                                    minval++;
                                    secval=0;
                                    mlsecval=0;
                                }
                                if(minval>60)
                                {
                                    hourval++;
                                    minval=0;
                                    secval=0;
                                    mlsecval=0;
                                }
                                hour.setText(hourval+"");
                                min.setText(""+minval);
                                sec.setText(""+secval);
                                mlsec.setText(""+mlsecval);

                            }
                            catch (Exception e)
                            {
                                System.out.println(e.getMessage());
                            }
                        }
                        else break;
                    }
                }

            };
            t.start();
            synchronized(t){
                t.notify();}
        }
        if(e.getSource()==b2)
        { state=false;
            b1state=false;
            synchronized(t) {
                try {
                    t.wait();
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            }
        }
        if(e.getSource()==b3)
        {
            hourval=0;
            minval=0;
            secval=0;
            mlsecval=0;
            hour.setText(hourval+"");
            min.setText(minval+"");
            sec.setText(secval+"");
            mlsec.setText(mlsecval+"");
        }
    }

    public static void main(String[] args) {
        new Chronometer();
    }
}
