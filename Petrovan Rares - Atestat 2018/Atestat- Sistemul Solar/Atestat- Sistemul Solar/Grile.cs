﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;
namespace Atestat__Sistemul_Solar
{
    public partial class Grile : UserControl
    {

        OleDbConnection conn; 
        List<intrebare> LI;
        public string id_test;
        int poz;
        string[] R;
      
        public Grile()
        {
            InitializeComponent();
        }
        private void afisare()
        {
            textBox1.Text = LI[poz].enunt;
            textBox2.Text = LI[poz].a;
            textBox3.Text = LI[poz].b;
            textBox4.Text = LI[poz].c;
            radioButton1.Checked = false;
            radioButton2.Checked = false;
            radioButton3.Checked = false;
            if (R[poz] == "A") radioButton1.Checked = true;
            if (R[poz] == "B") radioButton2.Checked = true;
            if (R[poz] == "C") radioButton3.Checked = true;
        }
        private void Grile_Load(object sender, EventArgs e)
        {
            
            string cs = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=Teste.accdb;Persist Security Info=True";
            conn = new OleDbConnection(cs);
            LI = new List<intrebare>();
            conn.Open();
            string q = "Select Intrebari.ID, Intrebari.Enunt, Intrebari.A, Intrebari.B, Intrebari.C, Intrebari.Corect, Intrebari.ID_Categorie FROM Intrebari  WHERE Intrebari.ID_Categorie=" + id_test;
            OleDbCommand c = new OleDbCommand(q, conn);
            OleDbDataReader dr = c.ExecuteReader();
           
            while (dr.Read())
            {
                intrebare b = new intrebare();
                b.id = int.Parse(dr[0].ToString());
                b.enunt = dr[1].ToString();
                b.a = dr[2].ToString();
                b.b = dr[3].ToString();
                b.c = dr[4].ToString();
                b.corect = dr[5].ToString();
               
                LI.Add(b);
            }
            R = new string[LI.Count];
            poz = 0;
            afisare();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (poz < LI.Count - 1)
            {
                poz ++;
                afisare();
            }
            else MessageBox.Show("Test finalizat, apasati  ''" + button3.Text  + "'' pentru a vesea rezultatul!");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (poz > 0)
            {
                poz--;
                afisare();
            }
        }
     

        private void button3_Click(object sender, EventArgs e)
        {
            int c = 0;
            for (int i = 0; i < LI.Count; i++)
            {
                
                if (LI[i].corect == R[i])
                    c++;
            }
            MessageBox.Show("Ati raspuns corect la " + c.ToString() +" din "+LI.Count+" intrebari");
        }

        private void radioButton1_CheckedChanged_1(object sender, EventArgs e)
        {
            if (radioButton1.Checked == true)
                R[poz] = "A";
        }

        private void radioButton2_CheckedChanged_1(object sender, EventArgs e)
        {
            if (radioButton2.Checked == true)
                R[poz] = "B";
        }

        private void radioButton3_CheckedChanged_1(object sender, EventArgs e)
        {
            if (radioButton3.Checked == true)
                R[poz] = "C";
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
