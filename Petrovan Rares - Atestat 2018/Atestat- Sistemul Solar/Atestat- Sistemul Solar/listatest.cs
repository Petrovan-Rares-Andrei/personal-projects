﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace Atestat__Sistemul_Solar
{
    public partial class listatest : UserControl
    {

        OleDbConnection conn;
        public string id;
        public string id_test;

        public listatest()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex != -1)
            {
                string s = listBox1.SelectedItem.ToString();
                string[] ss = s.Split();
                string id_test = ss[0];
                this.Controls.Clear();
              Grile c1 = new Grile();
                c1.id_test = id_test;
                this.Controls.Add(c1);
                this.Height = c1.Height + 80;
                this.Width = c1.Width + 80;
            }
        }

        private void listatest_Load(object sender, EventArgs e)
        {


            string cs = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=Teste.accdb;Persist Security Info=True";
            conn = new OleDbConnection(cs);
            conn.Open();
            string q = "SELECT * FROM teste";
            //MessageBox.Show(q);
            OleDbCommand c = new OleDbCommand(q, conn);
            OleDbDataReader dr = c.ExecuteReader();
            while (dr.Read())
            {
                string s = dr[0].ToString() + " " + dr[1].ToString();
                listBox1.Items.Add(s);
            }
        }
    }
}
