﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Atestat__Sistemul_Solar
{
    public partial class Form2 : Form
    {
        string[] imagefile;
        int n;
        int x, y;
        PictureBox[] P;
        int poz;
        int width, height;
        public Form2()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            poz++;
            if (poz >= n) poz = 0;
            pictureBox1.Load(imagefile[poz]);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            poz--;
            if (poz <= 0) poz = n-1;
            pictureBox1.Load(imagefile[poz]);
        }

        private void click(object sender, EventArgs e)
        {
            for (int i = 0; i < n; i++)
                if (sender == P[i])
                {
                    pictureBox1.Load(imagefile[i]);
                    poz = i;
                }
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            imagefile = Directory.GetFiles("Imagini", "*.jpg");
            n = imagefile.Count();
            y = x = this.Width/n-3;
            P = new PictureBox[n+1];
            pictureBox1.Top = y + 40;
            pictureBox1.Height = this.Height - y - 150;
            pictureBox1.Width = pictureBox1.Height *4/3 ;
            pictureBox1.Left = 10 + this.Width / 2 - pictureBox1.Width / 2;
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
          
            poz = 0;
           
            int i = 0;
            foreach (string s in imagefile)
            {
                P[i] = new PictureBox();
                P[i].Width = x;
                P[i].Height = y;
                P[i].Load(s);
                P[i].Top = 20;
                P[i].Left = (x + 5) * i + 5;
                P[i].SizeMode = PictureBoxSizeMode.StretchImage;
                this.Controls.Add(P[i]);
                P[i].Click += new EventHandler(click);
                i++;

                
            }
            pictureBox1.Load(imagefile[0]);
            width = pictureBox1.Width;
            height = pictureBox1.Height;

        }
    }
}
