﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atestat__Sistemul_Solar
{
    class intrebare
    {
        public string enunt, a, b, c, d, corect;
        public int id;

        public intrebare() { }
        public intrebare(int id, string enunt,  string a, string b, string c, string d, string corect)
        {
            this.id = id;
            this.enunt = enunt;
            this.a = a;
            this.b = b;
            this.c = c;
            this.d = d;
            this.corect = corect;
            
        }
    }
}
