﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;

namespace Daily_Caloric_Intake_Calculator
{ 


    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            panel2.Visible = true;
            pictureBox2.Visible = true;
            panel3.Visible = false;
        }

        private void button4_Click(object sender, EventArgs e)
        {  
            panel2.Visible = false;
            panel3.Visible = true;
            pictureBox2.Visible = true;
          
        }

        private void button2_Click(object sender, EventArgs e)
        {
           
            string fname = textBox3.Text, lname = textBox4.Text, username = textBox5.Text, pswd = textBox6.Text;
            if (fname.Equals("") || lname.Equals("") || username.Equals("") || pswd.Equals("")) MessageBox.Show("Invalid Field Value!");
            else
            {   MySqlConnection con = new MySqlConnection("Data Source = localhost; UserId = root; database =fitnessapp");
                con.Open();
                MySqlCommand ins = new MySqlCommand("", con);
                ins.CommandText = "INSERT INTO `usersdata` (`FirstName`, `LastName`, `Username`, `Password`) VALUES ('" + fname + "', '" + lname + "', '" + username + "', '" + pswd + "');";
                MySqlTransaction trans = con.BeginTransaction();
                try
                {
                    ins.Transaction = trans;
                    ins.ExecuteNonQuery();
                    trans.Commit();
                }
                catch
                {

                    trans.Rollback();
                    MessageBox.Show("Database error!");
                }
                panel3.Visible = false;
                panel2.Visible = true;
            }
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            panel2.Visible = false;
            panel3.Visible = false;
            pictureBox2.Visible = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {           
            try
            { 
                string username = textBox1.Text, pswd = textBox2.Text;
                MySqlConnection con = new MySqlConnection("Data Source = localhost; UserId = root; database =fitnessapp");
                con.Open();
                MySqlCommand sel = new MySqlCommand("", con);
                sel.CommandText="Select * from `usersdata`  where Username='"+username+"' and  Password='"+pswd+"';";
                MySqlDataReader myreader = sel.ExecuteReader();
                if (myreader.Read())
                {
                    Form2 F2 = new Form2(myreader.GetString(1), myreader.GetString(2));
                    F2.Show();
                    this.Hide();
                }
                else
                {
                    MessageBox.Show("Incorrect user or password!\nIf you dont have an account please sign-up first!");

                }
            }
            catch {

                MessageBox.Show("Database interrogation error!");
            }
        }
        private void pictureBox3_Click(object sender, EventArgs e)
        {
           
            Application.Exit();
        }
    }
}
