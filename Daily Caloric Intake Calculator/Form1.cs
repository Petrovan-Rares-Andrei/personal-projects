﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Daily_Caloric_Intake_Calculator
{
    public partial class Form1 : Form
    {
        int progress = 0;
        public Form1()
        {
            InitializeComponent();
            circularProgressBar1.Value = 0;
            circularProgressBar1.Minimum = 0;
            circularProgressBar1.Maximum = 100;
            timer1.Start();           
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            progress++;

            if (progress <= 100)
            {  
                if (progress == 100) {
                   
                    label1.SetBounds(label1.Bounds.X - 6, label1.Bounds.Y, label1.Bounds.Width, label1.Bounds.Height);   
                }
                circularProgressBar1.Value = progress;
                circularProgressBar1.Update();
                label1.Text = progress.ToString() + "%";
              
                Thread.Sleep(20);
            }
          
            if(progress==105)
            {
                timer1.Enabled = false;
                Form3 F3 = new Form3();
                F3.Show();
                this.Hide();
            }                   
        }       
    }
}
