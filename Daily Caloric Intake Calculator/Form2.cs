﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Daily_Caloric_Intake_Calculator
{
    public partial class Form2 : Form
    {
        double kcals = 0, p = 0, c = 0, f = 0;
        string[] quotes = { "“Health is a state of complete \n harmony of the body, mind and spirit...",
                            "“To ensure good health: \n eat lightly, breathe deeply, \n live moderately, cultivate cheerfulness, \n and maintain an interest in life.”",
                            "“Physical fitness is the first \n requisite of happiness.”",
                            "“It is health that is the real wealth, \n and not pieces of gold and silver.”",
                            "“It is exercise alone that supports the spirits, \n and keeps the mind in vigor.”"};
        public Form2(string fname, string lname)
        {
            InitializeComponent();
            label1.Text = fname + " " + lname;
            label16.Text = 0 + " kcal";
            Random r = new Random();
            int i = r.Next(0, 4);
            label17.Text = "Quote of the day:\n"+ quotes[i];
            circularProgressBar1.Value = 0;
            circularProgressBar2.Value = 0;
            circularProgressBar3.Value = 0;
            circularProgressBar4.Value = 0;
            circularProgressBar1.Update();
            circularProgressBar2.Update();
            circularProgressBar3.Update();
            circularProgressBar4.Update();
        }

        private void button1_Click(object sender, EventArgs e)///Macronutrients button
        {
                panel2.Visible = true;
                panel1.Visible = false;
                timer1.Start();          
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Form3 F3 = new Form3();
            F3.Show();
            this.Hide();
        }

        private void button3_Click(object sender, EventArgs e)//log out button
        {
            Application.Exit();
        }    
        private void timer1_Tick(object sender, EventArgs e)
        {          
            label9.Text = Math.Round(kcals).ToString() + "kcals";
            label3.Text = p.ToString() + "g";
            label5.Text = c.ToString() + "g";
            label7.Text = f.ToString() + "g";
            circularProgressBar1.Value = (int)p;
            circularProgressBar2.Value = (int)c;
            circularProgressBar3.Value = (int)f;
            circularProgressBar4.Value = (int)kcals;
            circularProgressBar1.Update();
            circularProgressBar2.Update();
            circularProgressBar3.Update();
            circularProgressBar4.Update();
            timer1.Enabled = false;           
        }
        private void button2_Click(object sender, EventArgs e)///Calorie calculator button
        {
            panel1.Visible = true;
            panel2.Visible = false;         
        }
        private void button4_Click_2(object sender, EventArgs e)//calculate button
        {            
            try
            {
                double BMR = 0, W = double.Parse(textBox3.Text), H = double.Parse(textBox2.Text), A = double.Parse(textBox1.Text);
                if (comboBox1.SelectedIndex.Equals(0))
                {
                    BMR = 13.397 * W + 4.799 * H - 5.677 * A + 88.362;                   
                }
                else
                {
                    BMR = 9.247 * W + 3.098 * H - 4.330 * A + 447.593;                                      
                }
                int activity = comboBox2.SelectedIndex;
                switch (activity)
                {
                    case 0: kcals = BMR * 1.2; break;
                    case 1: kcals = BMR * 1.375; break;
                    case 2: kcals = BMR * 1.55; break;
                    case 3: kcals = BMR * 1.725; break;
                    case 4: kcals = BMR * 1.9; break;
                }               
            }
            catch { MessageBox.Show("Invalid Field Value!"); }
            p = Math.Round((0.25 * kcals) / 4);
            c = Math.Round((0.45 * kcals) / 4);
            f = Math.Round((0.3 * kcals) / 9);
            if (p > circularProgressBar1.Maximum || c > circularProgressBar2.Maximum || f > circularProgressBar3.Maximum || kcals > circularProgressBar4.Maximum)
            {
                MessageBox.Show("Result exceeding the accepted values range!");
            }
            else
            {
                label16.Text = Math.Round(kcals) + " kcal";
                button1.Enabled = true;    
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedIndex.Equals(0))
            {
                pictureBox3.Visible = true;
                pictureBox5.Visible = false;
                label17.Visible = false;
            }
            else
            {
                pictureBox5.Visible = true;
                pictureBox3.Visible = false;
                label17.Visible = false;
            }
        }
      }
    }


